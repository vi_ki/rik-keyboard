"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Input_1 = require("./components/input/Input");
exports.Input = Input_1.default;
var Button_1 = require("./components/button/Button");
exports.Button = Button_1.default;
var Key_1 = require("./components/keys/Key");
exports.Key = Key_1.default;
var KeyBackSpace_1 = require("./components/keys/KeyBackSpace");
exports.KeyBackSpace = KeyBackSpace_1.default;
var KeyCancel_1 = require("./components/keys/KeyCancel");
exports.KeyCancel = KeyCancel_1.default;
var KeyLetter_1 = require("./components/keys/KeyLetter");
exports.KeyLetter = KeyLetter_1.default;
require("./index.css");
var Keyboard = /** @class */ (function (_super) {
    __extends(Keyboard, _super);
    function Keyboard(props) {
        var _this = _super.call(this, props) || this;
        _this._letterAdded = _this._letterAdded.bind(_this);
        return _this;
    }
    Keyboard.prototype._backspacePressed = function () {
        var inputNode = this.props.inputNode;
        var value = inputNode.value, selectionStart = inputNode.selectionStart, selectionEnd = inputNode.selectionEnd;
        var nextValue = 0;
        var nextSelectionPosition = 0;
        if (selectionStart === selectionEnd) {
            nextValue = value.substring(0, selectionStart - 1) + value.substring(selectionEnd);
            nextSelectionPosition = selectionStart - 1;
        }
        else {
            nextValue = value.substring(0, selectionStart) + value.substring(selectionEnd);
            nextSelectionPosition = selectionStart;
        }
        nextSelectionPosition = nextSelectionPosition > 0 ? nextSelectionPosition : 0;
        inputNode.value = nextValue;
        setTimeout(function () {
            inputNode.focus();
            inputNode.setSelectionRange(nextSelectionPosition, nextSelectionPosition);
        }, 0);
        inputNode.dispatchEvent(new Event('input', { bubbles: true }));
    };
    Keyboard.prototype._letterAdded = function (letter) {
        var input = this.props.inputNode;
        var value = input.value, selectionStart = input.selectionStart, selectionEnd = input.selectionEnd;
        var nextValue = value.substring(0, selectionStart) + letter + value.substring(selectionEnd);
        nextValue = nextValue.replace(/^ /, '');
        nextValue = nextValue.replace(/ +/g, ' ');
        input.value = nextValue;
        setTimeout(function () {
            input.focus();
            input.setSelectionRange(selectionStart + 1, selectionStart + 1);
        }, 0);
        input.dispatchEvent(new Event('input', { bubbles: true }));
    };
    Keyboard.prototype.render = function () {
        var _this = this;
        var childrenWithProps = React.Children.map(this.props.children, function (child) { return React.cloneElement(child, {
            letterAdded: _this._letterAdded.bind(_this),
            backspacePressed: _this._backspacePressed.bind(_this)
        }); });
        return React.createElement("div", { className: 'key-container' }, childrenWithProps);
    };
    return Keyboard;
}(React.Component));
exports.Keyboard = Keyboard;
