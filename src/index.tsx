import * as React from 'react';
import Input from './components/input/Input';
import Button from './components/button/Button';
import Key from './components/keys/Key';
import KeyBackSpace from './components/keys/KeyBackSpace';
import KeyCancel from './components/keys/KeyCancel';
import KeyLetter from './components/keys/KeyLetter';
import "./index.css";


export interface IKeyboardProps { inputNode: any; children: any }
export interface IKeyboardState { }

class Keyboard extends React.Component<IKeyboardProps, Partial<IKeyboardState>> {

    constructor(props:any) {
        super(props);
        this._letterAdded = this._letterAdded.bind(this);
    }

    _backspacePressed(): void {
        let inputNode = this.props.inputNode;
        let value = inputNode.value,
            selectionStart = inputNode.selectionStart,
            selectionEnd = inputNode.selectionEnd;

        let nextValue =  0;
        let nextSelectionPosition =  0;
        if (selectionStart === selectionEnd) {
            nextValue = value.substring(0, selectionStart - 1) + value.substring(selectionEnd);
            nextSelectionPosition = selectionStart - 1;
        } else {
            nextValue = value.substring(0, selectionStart) + value.substring(selectionEnd);
            nextSelectionPosition = selectionStart;
        }
        nextSelectionPosition = nextSelectionPosition > 0 ? nextSelectionPosition : 0;

        inputNode.value = nextValue;
        setTimeout(function () {
            inputNode.focus();
            inputNode.setSelectionRange(nextSelectionPosition, nextSelectionPosition);
        }, 0);
        inputNode.dispatchEvent(new Event('input', { bubbles: true }));
    }

    _letterAdded(letter:string) {
        let input = this.props.inputNode;
        let value = input.value,
            selectionStart = input.selectionStart,
            selectionEnd = input.selectionEnd;
        let nextValue = value.substring(0, selectionStart) + letter + value.substring(selectionEnd);
        nextValue = nextValue.replace(/^ /, '');
        nextValue = nextValue.replace(/ +/g, ' ');
        input.value = nextValue;
        setTimeout(function () {
            input.focus();
            input.setSelectionRange(selectionStart + 1, selectionStart + 1);
        }, 0);
        input.dispatchEvent(new Event('input', { bubbles: true }));
    }

    render() {
        const childrenWithProps = React.Children.map(this.props.children,
            (child:any) => React.cloneElement(child, {
                letterAdded: this._letterAdded.bind(this),
                backspacePressed: this._backspacePressed.bind(this)
            })
        );
        return <div className={'key-container'}>{childrenWithProps}</div>
    }
}
export { Keyboard, Input, Button, Key, KeyLetter, KeyBackSpace, KeyCancel }
