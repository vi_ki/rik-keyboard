"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Button_1 = require("../../components/button/Button");
var Key = /** @class */ (function (_super) {
    __extends(Key, _super);
    function Key(props) {
        return _super.call(this, props) || this;
    }
    Key.prototype.render = function () {
        return (React.createElement(Button_1.default, { action: this.props.action, styleName: this.props.class }));
    };
    return Key;
}(React.Component));
exports.Key = Key;
exports.default = Key;
