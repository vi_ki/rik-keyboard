"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Button_1 = require("../../components/button/Button");
var KeyCancel = /** @class */ (function (_super) {
    __extends(KeyCancel, _super);
    function KeyCancel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    KeyCancel.prototype.render = function () {
        return (React.createElement(Button_1.default, { action: this.props.cancel, styleName: this.props.class, text: 'c' }));
    };
    return KeyCancel;
}(React.Component));
exports.KeyCancel = KeyCancel;
exports.default = KeyCancel;
