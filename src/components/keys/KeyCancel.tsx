import * as React from 'react';
import Button from '../../components/button/Button';

export class KeyCancel extends React.Component<any, any> {

    render() {
        return (
            <Button action={this.props.cancel} styleName={this.props.class} text={'c'}/>
        );
    }
}

export default KeyCancel;