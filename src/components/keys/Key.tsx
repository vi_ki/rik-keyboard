import * as React from 'react';
import Button from '../../components/button/Button';

export class Key extends React.Component<any, any> {


    constructor(props:any) {
        super(props);
    }

    render() {
        return (
            <Button action={this.props.action} styleName={this.props.class}/>
        );
    }
}

export default Key;