import * as React from 'react';
import Button from '../../components/button/Button';

export class KeyBackSpace extends React.Component<any, any> {


    render() {
        return (
            <Button action={this.props.backspacePressed} styleName={this.props.class} text={'c'}/>
        );
    }
}

export default KeyBackSpace;