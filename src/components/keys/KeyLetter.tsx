import * as React from 'react';
import Button from '../../components/button/Button';


export class KeyLetter extends React.Component<any, any> {

    constructor(props:any) {
        super(props);
    }


    render() {
        return (
            <Button action={()=>{this.props.letterAdded(this.props.value)}} styleName={this.props.class} text={this.props.value}/>
        );
    }
}

export default KeyLetter;