import * as React from 'react';

export interface InputProps { value: string; onFocus: any; onChange:any; styleName:any}
export interface InputState { }

export class Input extends React.Component<InputProps, Partial<InputState>> {

    handleInput = (event:any) => this.props.onChange(event.target.value);

    handleFocus = () => {
        if (this.props.onFocus) {
            this.props.onFocus(this.refs.input);
            // the `this.refs.input` value should be passed to the Keyboard component as inputNode prop
        }
    };

    componentDidMount(){
        this.handleFocus()
    }

    render() {
        const {value} = this.props;

        return (
            <input
                type={'text'}
                onInput={this.handleInput}
                value={value}
                onFocus={this.handleFocus}
                ref="input"
                className={this.props.styleName ? this.props.styleName : null}
            />
        );
    }
}

export default Input;