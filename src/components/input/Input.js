"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Input = /** @class */ (function (_super) {
    __extends(Input, _super);
    function Input() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.handleInput = function (event) { return _this.props.onChange(event.target.value); };
        _this.handleFocus = function () {
            if (_this.props.onFocus) {
                _this.props.onFocus(_this.refs.input);
                // the `this.refs.input` value should be passed to the Keyboard component as inputNode prop
            }
        };
        return _this;
    }
    Input.prototype.componentDidMount = function () {
        this.handleFocus();
    };
    Input.prototype.render = function () {
        var value = this.props.value;
        return (React.createElement("input", { type: 'text', onInput: this.handleInput, value: value, onFocus: this.handleFocus, ref: "input", className: this.props.styleName ? this.props.styleName : null }));
    };
    return Input;
}(React.Component));
exports.Input = Input;
exports.default = Input;
