import * as React from 'react';

export interface IButtonProps { text: any; value:any; action: void|null; className:any}
export interface IButtonState { text: any; value:any; action: void|null; className:any}

export class Button extends React.Component<any, any> {

    constructor(props:any) {
        super(props);
        this.state = {
            text: '',
            value: '',
            action: null,
            styleName: 'key'
        }
    }

    componentWillMount() {
        this.setState(
            {
                text: this.props.text,
                value: this.props.value,
                styleName: this.props.styleName ? this.props.styleName : this.state.styleName,
                action: this.props.action
            }
        );
    }


    render() {
        return (
            <li className={this.state.styleName} onMouseDown={this.state.action}>
                <div >
                    <span>{this.props.text}</span>
                </div>
            </li>

        );
    }
}

export default Button;