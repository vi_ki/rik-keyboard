#### Provides a react component for a specific project named 'RIK'

### Quick start

Installing via npm
```
npm install rik-keyboard
```
## Examples

```ts
import {
 Keyboard,
 Input,
 Button,
 Key,
 KeyLetter,
 KeyBackSpace,
 KeyCancel} from 'rik-keyboard';

 getInitialState() {
   return {ref: null, value: ''}
 },

 yourMethod(){
   console.log('action key')
 }

 render() {
     return (
        <div className="keyboard">
          <Input value={this.state.value} styleName={'your_class_name'} onChange={this.change} onFocus={this.onFocus}/>
          <ul className='keyHolder'>
             <Keyboard inputNode={this.state.ref}>

                /**Inserting key components  */

                 <KeyLetter value='7' class={'your_class_name'}/>
                 <KeyBackSpace class={'your_class_name'}/>
                 <KeyCancel class={'your_class_name'}/>

               /**transfer your method  */

                 <Key action={this.yourMethod} class={'your_class_name'}/>
               </Keyboard>
              </ul>
        </div>
        );
    }